package ru.t1.vsukhorukova;

import io.reactivex.Observable;
import io.vertx.core.http.HttpServerRequest;
import ru.t1.vsukhorukova.exception.ParameterInputLanguageNotFoundException;
import ru.t1.vsukhorukova.exception.ParameterOutputLanguageNotFoundException;
import ru.t1.vsukhorukova.exception.ParameterTextNotFoundException;
import ru.t1.vsukhorukova.model.RequestModel;
import ru.t1.vsukhorukova.model.WordModel;
import ru.t1.vsukhorukova.repository.RequestRepository;
import ru.t1.vsukhorukova.repository.WordRepository;
import ru.t1.vsukhorukova.util.Translator;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/main")
@RequestScoped
public class Main {

    @Inject
    RequestRepository requestRepository = new RequestRepository();

    @Inject
    WordRepository wordRepository;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello!";
    }

    @GET
    @Transactional
    @Path("/translate")
    public List<WordModel> translate(@Context HttpServerRequest request) throws Exception {
        String inputLanguage = request.getParam("input-language");
        String outputLanguage = request.getParam("output-language");
        String text = request.getParam("text");

        if (text == null || text.isEmpty()) throw new ParameterTextNotFoundException();
        if (inputLanguage == null || inputLanguage.isEmpty()) throw new ParameterInputLanguageNotFoundException();
        if (outputLanguage == null || outputLanguage.isEmpty()) throw new ParameterOutputLanguageNotFoundException();

        RequestModel requestModel = new RequestModel();
        requestModel.setInputLanguage(inputLanguage);
        requestModel.setOutputLanguage(outputLanguage);
        requestModel.setInputText(text);
        requestModel.setIp(request.remoteAddress().toString());
        requestRepository.persist(requestModel);

        Observable.just(text)
                .flatMap(str -> Observable.fromArray(str.split(" ")))
                .subscribe(
                        inputWord -> {
                            String outputWord = Translator.translate(inputLanguage, outputLanguage, inputWord);
                            requestModel.getWords().add(new WordModel(requestModel, inputWord, outputWord));
                        });

        return requestModel.getWords();
    }

    @GET
    @Path("/show_requests")
    public List<RequestModel> showAllRequests() {
        return requestRepository.findAll().list();
    }

    @GET
    @Path("/show_words")
    public List<WordModel> showAllWords() {
        return wordRepository.findAll().list();
    }

}