package ru.t1.vsukhorukova.exception;

public class ParameterTextNotFoundException extends Exception {

    public ParameterTextNotFoundException() {
        super("Error! The parameter text isn't found...");
    }

}
