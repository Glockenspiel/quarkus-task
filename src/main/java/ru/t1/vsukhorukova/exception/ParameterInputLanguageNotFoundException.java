package ru.t1.vsukhorukova.exception;

public class ParameterInputLanguageNotFoundException extends Exception {

    public ParameterInputLanguageNotFoundException() {
        super("Error! The input-language text isn't found...");
    }

}
