package ru.t1.vsukhorukova.exception;

public class ParameterOutputLanguageNotFoundException extends Exception {

    public ParameterOutputLanguageNotFoundException() {
        super("Error! The output-language text isn't found...");
    }

}
