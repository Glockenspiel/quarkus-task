package ru.t1.vsukhorukova.util;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Translator {

    public static String translate (
            String inputLanguage,
            String outputLanguage,
            String word
    ) throws InterruptedException, IOException {
        String url = "https://translated-mymemory---translation-memory.p.rapidapi.com/api/get";
        String languageParam = "langpair=" + inputLanguage + "%7C" + outputLanguage;
        String textParam = "q=" + word;
        String otherParams = "mt=1&onlyprivate=0&de=a%40b.c";
        String requestTranslateStr = url + '?' + languageParam + '&' + textParam + '&' + otherParams;

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(requestTranslateStr))
                .header("X-RapidAPI-Key", "f7f7892379msh25077f558e2e5cfp154d41jsn1fe863202d93")
                .header("X-RapidAPI-Host", "translated-mymemory---translation-memory.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        String responseStr = response.body();
        int beginIndex = responseStr.indexOf("\"translation\":") + 15;
        int endIndex = responseStr.indexOf("\"source\":") - 2;
        return responseStr.substring(beginIndex, endIndex);
    }

}
