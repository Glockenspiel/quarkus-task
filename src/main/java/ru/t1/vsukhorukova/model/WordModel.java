package ru.t1.vsukhorukova.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "word")
public class WordModel {

    @Id
    @Column
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(name = "request_id")
    private RequestModel requestModel;

    @Column(name = "input_word")
    private String inputWord;

    @Column(name = "output_word")
    private String outputWord;

    public WordModel() {
    }

    public WordModel(RequestModel requestModel, String inputWord, String outputWord) {
        this.requestModel = requestModel;
        this.inputWord = inputWord;
        this.outputWord = outputWord;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRequest(RequestModel requestModel) {
        this.requestModel = requestModel;
    }

    public RequestModel getRequest() {
        return requestModel;
    }

    public String getInputWord() {
        return inputWord;
    }

    public void setInputWord(String inputWord) {
        this.inputWord = inputWord;
    }

    public String getOutputWord() {
        return outputWord;
    }

    public void setOutputWord(String outputWord) {
        this.outputWord = outputWord;
    }

    @Override
    public String toString() {
        return "Word { input = '" + inputWord + "', output = '" + outputWord + "' }";
    }

}
