package ru.t1.vsukhorukova.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "request")
public class RequestModel {

    @Id
    @Column
    private String id = UUID.randomUUID().toString();

    @Column
    private Date created = new Date();;

    @Column(name = "input_language")
    private String inputLanguage;

    @Column(name = "output_language")
    private String outputLanguage;

    @Column(name = "input_text")
    private String inputText;

    @Column
    private String ip;

    @OneToMany(mappedBy = "requestModel", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<WordModel> wordModels = new ArrayList<>();

    public RequestModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getInputLanguage() {
        return inputLanguage;
    }

    public void setInputLanguage(String inputLanguage) {
        this.inputLanguage = inputLanguage;
    }

    public String getOutputLanguage() {
        return outputLanguage;
    }

    public void setOutputLanguage(String outputLanguage) {
        this.outputLanguage = outputLanguage;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<WordModel> getWords() {
        return wordModels;
    }

    public void setWords(List<WordModel> wordModels) {
        this.wordModels = wordModels;
    }

}
