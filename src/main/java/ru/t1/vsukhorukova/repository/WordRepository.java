package ru.t1.vsukhorukova.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ru.t1.vsukhorukova.model.WordModel;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class WordRepository implements PanacheRepository<WordModel> {
}
