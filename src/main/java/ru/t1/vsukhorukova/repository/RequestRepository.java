package ru.t1.vsukhorukova.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ru.t1.vsukhorukova.model.RequestModel;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RequestRepository implements PanacheRepository<RequestModel> {
}
