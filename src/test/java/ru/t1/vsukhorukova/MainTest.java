package ru.t1.vsukhorukova;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class MainTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/main")
          .then()
             .statusCode(200)
             .body(is("Hello from RESTEasy Reactive"));
    }

}